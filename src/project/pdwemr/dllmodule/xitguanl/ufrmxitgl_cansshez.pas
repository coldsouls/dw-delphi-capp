unit ufrmxitgl_cansshez;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbnavbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery,
  ufrmmdichildbase, ExtCtrls, Buttons, UpSpeedButton, UPanel, StdCtrls,
  ComCtrls, UpPageControl, UpComboBox, UpLabel, Grids, Wwdbigrd, Wwdbgrid,
  utypes, UpWWDbGrid, CnSpin, UpGroupBox, UpDateTimePicker, UpCheckBox,
  frxExportXLS, frxExportPDF, RzTabs, ImgList, RzButton, RzPanel,
  ToolPanels, UpAdvToolPanel, UpAdoStoreProc;

type
  Tfrmxitgl_cansshez = class(Tfrmdbnavbase)
    dlgColor: TColorDialog;
    pg: TRzPageControl;
    rztbshtTabSheet1: TRzTabSheet;
    tsglobalparams: TRzTabSheet;
    pbdbuttons: TUPanel;
    btnbdsave: TUpSpeedButton;
    btnbdrefresh: TUpSpeedButton;
    pglbuttons: TUPanel;
    btnglsave: TUpSpeedButton;
    btnglrefresh: TUpSpeedButton;
    dbggl: TUpWWDbGrid;
    lbl1: TLabel;
    dlgOpen: TOpenDialog;
    cbbpdfver: TUpComboBox;
    lbl2: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnbdsaveClick(Sender: TObject);
    procedure btnbdrefreshClick(Sender: TObject);
    procedure btnglrefreshClick(Sender: TObject);
    procedure btnglsaveClick(Sender: TObject);
    procedure txtzhengcsClick(Sender: TObject);
  private
    //初始化页面                        
    function initLocalPage() : Boolean;
    function initGlobalPage() : Boolean;

    //加载参数值
    function loadLocalParams() : Boolean;
    function loadGlobalParams() : Boolean; overload;
    function loadGlobalParams(pchk : PUpCheckBox; cansm : string) : Boolean; overload;

    //保存参数值
    function saveLocalParams() : Boolean;
    function saveGlobalParams() : Boolean; overload;
    function saveGlobalParams(pchk : PUpCheckBox; cansm : string) : Boolean; overload;

  protected
    function Execute_dll_show_before() : Boolean; override;

  public
  
  end;
var
  frmxitgl_cansshez: Tfrmxitgl_cansshez;

implementation

uses ufrmdbbase, ufrmbase;

{$R *.dfm}

function Tfrmxitgl_cansshez.Execute_dll_show_before: Boolean;
begin
  inherited Execute_dll_show_before();

  { 初始化页面 }
  initGlobalPage;
  initLocalPage;

  { 加载参数 }
  loadGlobalParams;
  loadLocalParams;

  { 非管理员admin用户隐藏全局参数设置面板
    2012-8-15 add by changym;
  }
  if dllparams.mysystem^.loginyuang.bianm <> 0 then
  begin
    tsglobalparams.TabVisible := False;
  end;

  Result := True;
end;

procedure Tfrmxitgl_cansshez.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  frmxitgl_cansshez := nil;
end;


function Tfrmxitgl_cansshez.initLocalPage: Boolean;
begin
  dllparams.mysystem^.local_sysparams.additem('acrobat_pdf_ver', dllparams.mysystem^.mainconfig.ReadString('localparams', 'acrobat_pdf_ver',''));
  Result := true;
end;

function Tfrmxitgl_cansshez.loadGlobalParams: Boolean;
begin
  tbl.Close;
  tbl.open;

  Result := True;
end;

function Tfrmxitgl_cansshez.loadLocalParams: Boolean;
begin
  cbbpdfver.ItemIndex:=
    cbbpdfver.Items.IndexOf(dllparams.mysystem^.local_sysparams.valueitem('acrobat_pdf_ver'));
  
  Result := True;
end;

function Tfrmxitgl_cansshez.saveGlobalParams: Boolean;
var
  strtemp : string;
begin
  //参数值合法性检查
  tbl.first;
  while not tbl.eof do
  begin
    {急诊费}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'mz_jizf') then
    begin
      if not baseobj.strisfloat(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数急诊费配置错误！');
        Exit;
      end;
    end;

    {病历工本费 mz_binglgbf}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'mz_binglgbf') then
    begin
      if not baseobj.strisfloat(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数病历工本费配置错误！');
        Exit;
      end;
    end;
    {卡费 mz_kaf}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'mz_kaf') then
    begin
      if not baseobj.strisfloat(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数卡费配置错误！');
        Exit;
      end;
    end;
    {挂号单有效期 mz_guahdyxq}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'mz_guahdyxq') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数挂号单有效期配置错误！');
        Exit;
      end;
    end;
    {处方单有效期 mz_chufdyxq}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'mz_chufdyxq') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数处方单有效期配置错误！');
        Exit;
      end;
    end;
    {用法默认每日次数 yongf_mormeircis}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'yongf_mormeircis') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数用法默认每日次数配置错误！');
        Exit;
      end;
    end;
    {门诊允许退药时长 menz_tuiy_shic}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'menz_tuiy_shic') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数门诊允许退药时长配置错误！');
        Exit;
      end;
    end;
    {门诊允许退费时长 menz_tuif_shic}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'menz_tuif_shic') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数门诊允许退费时长配置错误！');
        Exit;
      end;
    end;
    {住院默认欠费额度 zhuy_qianfed_default}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_qianfed_default') then
    begin
      if not baseobj.strisfloat(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数住院默认欠费配置错误！');
        Exit;
      end;
    end;
    {住院默认催缴额度 zhuy_cuijed_default}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_cuijed_default') then
    begin
      if not baseobj.strisfloat(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数住院默认催缴额度配置错误！');
        Exit;
      end;
    end;
    {药品失效提前N天告警 g_yaop_shixgaoj_days}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'g_yaop_shixgaoj_days') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数药品失效提前N天告警配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点1 zhuy_tiwd_celsjd1}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd1') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点1配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点1配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点2 zhuy_tiwd_celsjd2}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd2') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点2配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点2配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点3 zhuy_tiwd_celsjd3} 
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd3') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点3配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点3配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点4 zhuy_tiwd_celsjd4} 
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd4') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点4配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点4配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点5 zhuy_tiwd_celsjd5} 
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd5') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点5配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点5配置错误！');
        Exit;
      end;
    end;
    {体温测试时间点6 zhuy_tiwd_celsjd6}   
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'zhuy_tiwd_celsjd6') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数体温测试时间点6配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 0) or
         (tbl.FieldByName('cansz').AsInteger > 24) then
      begin
        baseobj.showwarning('全局参数体温测试时间点6配置错误！');
        Exit;
      end;
    end;
    {住院号种子值 g_zhuyh_zhongz}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'g_zhuyh_zhongz') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数住院号初始值配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 1) or
         (tbl.FieldByName('cansz').AsInteger > 999999999) then
      begin
        baseobj.showwarning('全局参数住院号初始值配置错误！');
        Exit;
      end;
    end;
    {门诊号种子值 g_menzh_zhongz}
    if SameText(LowerCase(tbl.fieldbyname('cansmc').AsString), 'g_menzh_zhongz') then
    begin
      if not baseobj.strisint(tbl.fieldbyname('cansz').AsString) then
      begin
        baseobj.showwarning('全局参数门诊号种子值配置错误！');
        Exit;
      end;
      if (tbl.fieldbyname('cansz').AsInteger < 1) or
         (tbl.FieldByName('cansz').AsInteger > 999999999) then
      begin
        baseobj.showwarning('全局参数门诊号种子值配置错误！');
        Exit;
      end;
    end;

    tbl.Next;
  end;

  //提交修改
  tbl.UpdateBatch();

  { 修改全局参数
  }
    
  //给主窗体发送系统全局参数更新消息
  SendMessage(dllparams.papp^.MainForm.Handle, UPM_SYSPARAMS_CHANGED_GLOBAL, 0, 0);
  
  Result := True;
end;

function Tfrmxitgl_cansshez.saveLocalParams: Boolean;
begin
  Result := False;

  if cbbpdfver.ItemIndex=-1 then
  begin
    baseobj.showerror('请选择Acrobat PDF版本！');
    exit;
  end;
    
  //acrobat PDF版本号
  dllparams.mysystem.mainconfig.WriteString('localparams','acrobat_pdf_ver',
      cbbpdfver.Text);
  
  //给主窗体发送系统本地参数更新消息
  SendMessage(dllparams.papp^.MainForm.Handle, UPM_SYSPARAMS_CHANGED_LOCAL, 0, 0);

  Result := True;
end;

procedure Tfrmxitgl_cansshez.btnbdsaveClick(Sender: TObject);
begin
  inherited;

  saveLocalParams;
end;

procedure Tfrmxitgl_cansshez.btnbdrefreshClick(Sender: TObject);
begin
  inherited;

  loadLocalParams;
end;

procedure Tfrmxitgl_cansshez.btnglrefreshClick(Sender: TObject);
begin
  inherited;

  loadGlobalParams;
end;

procedure Tfrmxitgl_cansshez.btnglsaveClick(Sender: TObject);
begin
  inherited;

  saveGlobalParams;
end;

procedure Tfrmxitgl_cansshez.txtzhengcsClick(Sender: TObject);
begin
  inherited;

  if dlgColor.Execute then
    (Sender as TStaticText).Color := dlgColor.Color; 
end;

function Tfrmxitgl_cansshez.loadGlobalParams(pchk: PUpCheckBox;
  cansm: string): Boolean;
begin
  qry.OpenSql('select cansz from sys_xitcs where cansmc=''' + cansm + '''');
  if qry.RecordCount = 0 then qry.Close
  else
  begin
    if qry.getString(0) = '1' then
      pchk^.Checked := True
    else
      pchk^.Checked := False;
  end;
end;

function Tfrmxitgl_cansshez.saveGlobalParams(pchk: PUpCheckBox;
  cansm: string): Boolean;
begin    
  if pchk^.Checked then
    qry.ExecuteSql('update sys_xitcs set cansz=''1''' +
      ' where cansmc=''' + cansm + '''')
  else                                      
    qry.ExecuteSql('update sys_xitcs set cansz=''0''' +
      ' where cansmc=''' + cansm + '''');
end;

function Tfrmxitgl_cansshez.initGlobalPage: Boolean;
begin
  Result := True;
end;

end.
