//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  umyconfig.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      配置文件类，直接从TIniFile继承而来；
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit umyconfig;

interface    

uses
  Windows, SysUtils, Variants, Classes, ubase, utypes, inifiles;

type
  TMyConfig = class(TIniFile)
  private
  public  
  end;

implementation

end.
