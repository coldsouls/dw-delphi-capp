//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  udatamodule.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      数据模块；持有一个数据库连接对象TAdoConnection的实例;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udmconn;

interface

uses
  Windows, SysUtils, Classes, DB, ADODB, udbconfigfile, UpAdoConnection;

type
  Tdmconn = class(TDataModule)
    conn: TUpADOConnection;
    con1: TUpAdoConnection;
    procedure DataModuleDestroy(Sender: TObject);
  private
  public
  end;

var
  dmconn: Tdmconn;

implementation

{$R *.dfm}

{ Tdm }

procedure Tdmconn.DataModuleDestroy(Sender: TObject);
begin
  if conn.Connected then conn.Close;
  if con1.Connected then con1.Close;
  
  dmconn := nil;
end;

end.
