unit ufrmcxbb_xitgl_caozrz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmcxbb1DbgridbaseMDI, ImgList, frxExportXLS, frxClass,
  frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB,
  ADODB, UpAdoTable, UpAdoQuery, Buttons, UpSpeedButton, ExtCtrls,
  ToolPanels, UpAdvToolPanel, Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid,
  RzButton, RzPanel, StdCtrls, UpLabel, ComCtrls, AdvDateTimePicker,
  RzRadChk, Mask, RzEdit, RzBtnEdt, uprzbuttonedit;

type
  Tfrmcxbb_xitgl_caozrz = class(Tfrmcxbb1DbgridbaseMDI)
    chkcaozsj: TRzCheckBox;
    updtcaozsjbegin: TAdvDateTimePicker;
    lbl17: TUpLabel;
    updtcaozsjend: TAdvDateTimePicker;
    chkcaozy: TRzCheckBox;
    edtcaozy: tuprzbuttonedit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  protected
    function PrintBefore() : Boolean; override;

    function BuildSqlString() : Boolean; override;

  end;

var
  frmcxbb_xitgl_caozrz: Tfrmcxbb_xitgl_caozrz;

implementation

uses ufrmdbbase;

{$R *.dfm}

function Tfrmcxbb_xitgl_caozrz.BuildSqlString: Boolean;
var
  strsql: string;
begin
  result:= false;

  strsql:= ' where 1=1';
  
  //操作时间
  if chkcaozsj.Checked then
  begin
    if updtcaozsjbegin.DateTime>=updtcaozsjend.DateTime then
    begin
      baseobj.showwarning('开始时间不得大于结束时间！');
      updtcaozsjbegin.SetFocus;
      Exit;
    end;

    strsql:= strsql + Format(' and caozsj>=''%s'' and caozsj<=''%s''',
      [baseobj.getSelBeginDateTime(updtcaozsjbegin.DateTime),
       baseobj.getSelEndDateTime(updtcaozsjend.DateTime)]);
  end;

  //操作员
  if chkcaozy.Checked then
  begin
    if edtcaozy.Text<>'' then
    begin
      strsql:= strsql + Format(' and caozybm=%s',[edtcaozy.StrValue]);
    end;
  end;

  qryreport_m.SQL.Text:= 'select * from v_sys_riz_caoz ' + strsql;

  Result:= True;
end;

procedure Tfrmcxbb_xitgl_caozrz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  frmcxbb_xitgl_caozrz:= nil;
end;

function Tfrmcxbb_xitgl_caozrz.PrintBefore: Boolean;
begin
  Result:= inherited PrintBefore();
end;

end.
