//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  uglobal.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      全局对象声明区；包含一个TMySystem的实例;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit uglobal;

interface
uses
  SysUtils, forms, utypes, ADODB, umysystem;

var
  gmysystem : TMySystem;
  //子系统对象，在全局初始化的时候设置
  
implementation   

end.
