//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  uencode.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      加解密模块;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit uencode;

interface
uses
  Windows, SysUtils, Variants, Classes, ubase, utypes;

  type
    TEncode = class(TBase)
    private

    public
      class function encode_xor(const buffer : pchar; ilen : integer; desbuf : pchar; c : char) : boolean;
      //xor一下   
  end;

implementation

{ THisEncode }

class function TEncode.encode_xor(const buffer : pchar; ilen : integer; desbuf: pchar;
  c: char): boolean;
var
  i : integer;
begin
  for i:=0 to ilen do
  begin
    desbuf[i] := chr(byte((buffer[i])) xor byte(c));
  end;

  Result := True;
end;

end.
