inherited frmdbbase: Tfrmdbbase
  Left = 31
  Top = 19
  Width = 1002
  Height = 544
  Caption = 'frmdbbase'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 14
  object qry: TUpAdoQuery [0]
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=pdfreview;Data Source=127.0.0.1,7788'
    Parameters = <>
    Left = 840
    Top = 8
  end
  object tbl: TUpAdoTable [1]
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=12345;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=yibshis;Data Source=changym\sql2008'
    AfterInsert = tblAfterInsert
    AfterEdit = tblAfterEdit
    AfterDelete = tblAfterDelete
    Left = 904
    Top = 8
  end
  object ds: TDataSource [2]
    DataSet = tbl
    Left = 936
    Top = 8
  end
  object qry1: TUpAdoQuery [3]
    Parameters = <>
    Left = 872
    Top = 8
  end
  object qryreport_m: TUpAdoQuery [4]
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=pdfreview;Data Source=127.0.0.1,7788'
    LockType = ltBatchOptimistic
    AfterOpen = qryreport_mAfterOpen
    Parameters = <>
    Left = 832
    Top = 40
  end
  object dsreport_m: TUpDataSource [5]
    DataSet = qryreport_m
    Left = 864
    Top = 40
  end
  object qryreport_d: TUpAdoQuery [6]
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=pdfreview;Data Source=127.0.0.1,7788'
    LockType = ltBatchOptimistic
    AfterOpen = qryreport_dAfterOpen
    Parameters = <>
    Left = 904
    Top = 40
  end
  object dsreport_d: TUpDataSource [7]
    DataSet = qryreport_d
    Left = 936
    Top = 40
  end
  object qryreport_vars: TUpAdoQuery [8]
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 792
    Top = 40
  end
  object frxdbds_d: TfrxDBDataset [9]
    UserName = '2'
    CloseDataSource = False
    OpenDataSource = False
    Left = 865
    Top = 72
  end
  object frpusds: TfrxUserDataSet [10]
    UserName = 'frpusds'
    Left = 792
    Top = 72
  end
  object frxdsgnr1: TfrxDesigner [11]
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    Restrictions = []
    RTLLanguage = False
    Left = 752
    Top = 72
  end
  object frxdbds_m: TfrxDBDataset [12]
    UserName = '1'
    CloseDataSource = False
    OpenDataSource = False
    Left = 832
    Top = 72
  end
  object frpreport: TfrxReport [13]
    Version = '4.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbPageSetup, pbEdit]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.CreateDate = 41035.898821793980000000
    ReportOptions.LastChange = 41035.898821793980000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    StoreInDFM = False
    OnGetValue = frpreportGetValue
    Left = 720
    Top = 72
  end
  object qry2: TUpAdoQuery
    Parameters = <>
    Left = 808
    Top = 8
  end
  object frxexportpdf: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    PrintOptimized = False
    Outline = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Background = False
    Creator = 'FastReport (http://www.fast-report.com)'
    HTMLTags = True
    Left = 648
    Top = 72
  end
  object frxexportxls: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    ExportPictures = False
    AsText = False
    Background = False
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 680
    Top = 72
  end
  object qrylog: TUpAdoQuery
    Parameters = <>
    Left = 512
    Top = 40
  end
  object frxdbdtst_3: TfrxDBDataset
    UserName = '3'
    CloseDataSource = False
    OpenDataSource = False
    Left = 832
    Top = 105
  end
  object frxdbdtst_4: TfrxDBDataset
    UserName = '4'
    CloseDataSource = False
    OpenDataSource = False
    Left = 866
    Top = 105
  end
  object frxdbdtst_5: TfrxDBDataset
    UserName = '5'
    CloseDataSource = False
    OpenDataSource = False
    Left = 901
    Top = 106
  end
end
