//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  udbcheck.pas     
//                                          
//  创建: changym by 2011-11-28 
//        changup@qq.com                    
//  功能说明:
//      数据库操作规则检查类。主要完成唯一性、关联性等的检查
//  修改历史:
//                                            
//  版权所有 (C) 2011 by changym
//                                                       
//*******************************************************************

unit udbcheck;

interface
uses
  SysUtils, Math, Types, DateUtils, ADODB, ubase, utypes, udbbase;

type
  pdbcheck = ^tdbcheck;
  tdbcheck = class(TDbBase)
  public
    function FKFieldHasValue(strtable, strfield : string; ivalue : integer) : Boolean;
    //strtable表中strfield字段是否存在strvalue值的记录

    { 指定表中、指定字段、指定值是否存在 }
    function colvalueisexists(tablename, colname : string;
                colvalue : string) : Boolean; overload;
    function colvalueisexists(tablename, colname : string;
                colvalue : integer) : Boolean; overload;
    //附带附加的sql条件addwere
    function colvalueisexists(tablename, colname : string;
                colvalue : string; addwhere : string) : Boolean; overload;
    function colvalueisexists(tablename, colname : string;
                colvalue : integer; addwhere : string) : Boolean; overload;
    function RowIsExists(strsql : string) : Boolean;
    //行数据是否存在; 用于根据sql检查数据是否存在

  end;
  
implementation

{ tdbcheck }

function tdbcheck.colvalueisexists(tablename, colname,
  colvalue: string): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := 'select top 1 1 from ' + tablename +
        ' where ' + colname + '=''' + colvalue + '''';
  qry1.Open;
  if qry1.RecordCount > 0 then
  begin
    Result := True;
  end
  else
  begin
    Result := false;
  end;
  qry1.Close;
end;

function tdbcheck.colvalueisexists(tablename, colname: string;
  colvalue: integer): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := 'select top 1 1 from ' + tablename +
        ' where ' + colname + '=' + IntToStr(colvalue);
  qry1.Open;
  if qry1.RecordCount > 0 then
  begin
    Result := True;
  end
  else
  begin
    Result := false;
  end;
  qry1.Close;
end;

function tdbcheck.colvalueisexists(tablename, colname, colvalue,
  addwhere: string): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := 'select top 1 1 from ' + tablename +
        ' where ' + colname + '=''' + colvalue + ''' and ' + addwhere;
  qry1.Open;
  if qry1.RecordCount > 0 then
  begin
    Result := True;
  end
  else
  begin
    Result := false;
  end;
  qry1.Close;
end;

function tdbcheck.colvalueisexists(tablename, colname: string;
  colvalue: integer; addwhere: string): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := 'select top 1 1 from ' + tablename +
        ' where ' + colname + '=' + IntToStr(colvalue) + ' and ' + addwhere;
  qry1.Open;
  if qry1.RecordCount > 0 then
  begin
    Result := True;
  end
  else
  begin
    Result := false;
  end;
  qry1.Close;
end;

function tdbcheck.FKFieldHasValue(strtable, strfield : string;
    ivalue : integer): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := 'select top 1 2 from ' + strtable +
    ' where ' + strfield + '=' + IntToStr(ivalue);
  qry1.Open;
  Result := qry1.RecordCount > 0;
  qry1.Close;
end;

function tdbcheck.RowIsExists(strsql: string): Boolean;
begin
  qry1.Close;
  qry1.SQL.Text := strsql;
  qry1.Open;
  Result := (qry1.RecordCount > 0);
  qry1.Close;
end;

end.
