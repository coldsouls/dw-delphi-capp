unit ufrmdbtreebase_mdi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbnavbase, frxExportXLS, frxClass, frxExportPDF, Menus,
  UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB, ADODB, UpAdoTable,
  UpAdoQuery, ExtCtrls, Buttons, UpSpeedButton, ToolPanels, UpAdvToolPanel,
  Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid, UPanel, ImgList, ComCtrls,
  StdCtrls, CnEdit, UpCnedit, PerlRegEx, RzButton, RzPanel, Mask, RzEdit,
  RzSpnEdt, UpRzspinedit, UpLabel, UpAdoStoreProc;

type
  TfrmdbtreebaseMDI = class(Tfrmdbnavbase)
    qrytree: TUpAdoQuery;
    imglstimglisttree: TImageList;
    tvtree: TTreeView;
    p2: TPanel;
    p1: TUpAdvToolPanel;
    btn2: TUpSpeedButton;
    btn3: TUpSpeedButton;
    btn4: TUpSpeedButton;
    lbl4: TLabel;
    lbl3: TLabel;
    edtyonghbm: TUpCnedit;
    edtmc: TUpCnedit;
    p3: TUpAdvToolPanel;
    lbl2: TLabel;
    btnfind: TUpSpeedButton;
    edtcxmc: TEdit;
    pmain: TUPanel;
    lbl1: TUpLabel;
    edtpaixh: tuprzspinedit;
    procedure tvtreeChange(Sender: TObject; Node: TTreeNode);
    procedure tvtreeDblClick(Sender: TObject);
    procedure tvtreeEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure tvtreeGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btnfindClick(Sender: TObject);
    procedure edtcxmcKeyPress(Sender: TObject; var Key: Char);
    procedure btnrefreshClick(Sender: TObject);
  private
    nodelist : TStringList;
    //节点辅助存储，存储各行的书签
    //key=节点编码，value=节点对象TreeNode的实例

    fpregex: TPerlRegEx;
    //perl正则表达式对象
  public
    tablename : string;
    //树表名,使用前必须设置
    wherestr: string;
    //过滤条件 

  private
    procedure showtree();
    //根据querytree显示树控件
    procedure showtreenode();
    //显示querytree当前节点到树控件

    procedure currrecord_treeselected();
    //将当前记录置为当前选中节点
    function treefind(caption : string) : Boolean;
    //节点查找

    function addrootnode() : Boolean;
    //增加根节点
    function addchildnode() : Boolean;
    //添加下级节点
    procedure addnodeafter(); virtual;
    //增加节点后
    function updatenode() : boolean;
    //修改节点名称
    function deltree() : Boolean;
    //删除节点

  { 虚函数部分,需要子类来实现
  }
  protected
    function getnodename_showtreenode() : string; virtual;
    //显示节点时生成节点名称;方便子类中自定义节点显示名称
    function getnodename_addroot() : string; virtual;
    //增加根节点取节点名称
    function getnodename_addchild() : string; virtual;
    //增加子节点取节点名称
    function getnodename_updatenode() : String; virtual;
    //修改节点取节点名称

    function addrootnodecheck() : Boolean; virtual;
    //添加根节点验证
    function addchildnodecheck() : Boolean; virtual;
    //添加子节点验证
    function UpdateNodeCheck() : Boolean; virtual;
    //修改节点验证
    function deletecheck() : Boolean; virtual;
    //删除前的检查验证

    function AddRootNode_SetFields() : Boolean; virtual;
    //添加根节点设置字段值
    function AddChildNode_Setfields(fujdbm : integer) : Boolean; virtual;
    //添加子节点设置字段值
    function UpdateNode_SetFields(fujdbm : integer) : Boolean; virtual;
    //修改节点设置字段值

    function UpdateNodeBefore() : Boolean; virtual;
    //修改节点前
    function UpdateNodeAfter() : Boolean; virtual;
    //修改节点成功后,主要用于变动tree的显示

    procedure treechange(node : TTreeNode); virtual;
    //树控件当前节点改变，通知querytree移动当前记录,子类设置修改字段初始值

  protected
    function Execute_dll_show_before() : Boolean; override;

  public
    function getnodefullbm(nodeid : Integer) : string;
    //得到节点完整编码路径
    function getnodefullname(nodeid : integer) : string;
    //得到节点完整名称，追朔到根节点的名称
  end;

var
  frmdbtreebaseMDI: TfrmdbtreebaseMDI;

implementation

{$R *.dfm}

{ TfrmdbtreebaseMDI }

function TfrmdbtreebaseMDI.addchildnode: Boolean;
var
  pid: integer;   
  bianm : integer;
begin
  Result := False;

  try
    //父节点
    pid := qrytree.FieldByName('bianm').AsInteger;

    //检查名称是否已经存在，不允许同级别同名重复出现
    if self.dbcheck.colvalueisexists(tablename, 'mingc', getnodename_addchild, 'fujdbm=' + IntToStr(pid)) then
    begin
      errmsg := '同级节点名称[' + getnodename_addchild + ']已经存在！';
      Exit;
    end;

    qrytree.Append;
    AddChildNode_Setfields(pid);
    qrytree.post;
                  
    //更新当前节点的路径全称
    bianm := qrytree.getInteger('bianm');
    qrytree.Edit;
    qrytree.FieldByName('bianmqm').AsString := getnodefullbm(bianm) + '/';
    qrytree.FieldByName('mingcqm').AsString := getnodefullname(bianm) + '/';
    qrytree.Post;

    //将当前节点添加进树形控件
    showtreenode;
    Result := True;
  except
    on e:Exception do
    begin
      qrytree.Cancel;
      errmsg := e.Message;
      Result := False;
    end;
  end;
end;

function TfrmdbtreebaseMDI.AddChildNode_Setfields(
  fujdbm: integer): Boolean;
begin
  qrytree.FieldByName('fujdbm').AsInteger := fujdbm;
  qrytree.FieldByName('mingc').AsString := getnodename_addchild;
  qrytree.FieldByName('jianp').AsString := baseobj.get_jianpin(getnodename_addchild);
  Result := True;
end;

function TfrmdbtreebaseMDI.addchildnodecheck: Boolean;
begin
  Result := False;

  //名称是否正确
  if Trim(getnodename_addchild) = '' then
  begin
    baseobj.showmsg('系统提示', '请输入分类名称', 0);
    edtmc.SetFocus;
    Exit;
  end;

  //是否选择父节点
  if tvtree.Selected = nil then
  begin
    baseobj.showmsg('系统提示', '请选择父分类节点', 0);
    Exit;
  end;

  Result := True;
end;

function TfrmdbtreebaseMDI.addrootnode: Boolean;
var
  pid: integer;  
  bianm : integer;
begin
  Result := False;
  
  try
    //父节点id=0
    pid := 0;

    //检查名称是否已经存在，不允许同级别同名重复出现
    if self.dbcheck.colvalueisexists(tablename, 'mingc', getnodename_addroot, 'fujdbm=' + IntToStr(pid)) then
    begin
      errmsg := '同级节点名称[' + getnodename_addroot + ']已经存在！';
      Exit;
    end;

    //增加节点
    qrytree.Append;
    AddRootNode_SetFields();
    qrytree.post;
                   
    //更新当前节点的路径全称
    bianm := qrytree.getInteger('bianm');
    qrytree.Edit;
    qrytree.FieldByName('bianmqm').AsString := getnodefullbm(bianm) + '/';
    qrytree.FieldByName('mingcqm').AsString := getnodefullname(bianm) + '/';
    qrytree.Post;
    
    //将当前节点添加进树形控件
    showtreenode();

    Result := True;
  except
    on e:Exception do
    begin
      qrytree.Cancel;
      errmsg := e.Message;
      Result := False;
    end;
  end;
end;

function TfrmdbtreebaseMDI.AddRootNode_SetFields: Boolean;
begin
  qrytree.FieldByName('fujdbm').AsInteger := 0;
  qrytree.FieldByName('mingc').AsString := getnodename_addroot;
  qrytree.FieldByName('jianp').AsString := baseobj.get_jianpin(getnodename_addroot);
  Result := True;
end;

function TfrmdbtreebaseMDI.addrootnodecheck: Boolean;
begin
  Result := False;

  //用户编码允许不输入

  //名称不能为空
  if Trim(getnodename_addroot) = '' then
  begin
    baseobj.showmsg('系统提示', '请输入分类名称!', 0);
    edtmc.SetFocus;
    Exit;
  end;

  Result := True;
end;

procedure TfrmdbtreebaseMDI.currrecord_treeselected;
var
  index: integer;
  Node: TTreeNode;
begin
  //数据集是否打开
  if not qrytree.Active then Exit;

  //是否有数据
  if qrytree.Eof then Exit;

  Index := nodelist.IndexOf(qrytree.FieldByName('bianm').AsString);
  Node := TTreeNode(nodelist.Objects[Index]);
  tvtree.Selected := Node;
  tvtree.SetFocus;
end;

function TfrmdbtreebaseMDI.deletecheck: Boolean;
begin

  Result := True;
end;

function TfrmdbtreebaseMDI.deltree: Boolean;
var
  node : TTreeNode;
begin
  Result := False;

  //取得选中的节点
  node := tvtree.Selected;
  //没有选择节点，退出处理
  if node = nil then
  begin
    exit;
  end;

  //检查当前节点是否有儿子节点
  if Self.dbcheck.colvalueisexists(tablename, 'fujdbm', qrytree.fieldbyname('bianm').AsInteger) then
  begin                  
    errmsg := '当前分类包含有下级分类，不能删除，请先删除下级分类';
    Exit;
  end;

  //删除当前选择的节点
  qrytree.GotoBookmark(node.Data);
  nodelist.Delete(nodelist.IndexOf(qrytree.FieldByName('bianm').AsString));
  qrytree.Delete;
  node.Delete;            
  Result := True;

  currrecord_treeselected;
end;

function TfrmdbtreebaseMDI.Execute_dll_show_before: Boolean;
begin
  Result := inherited Execute_dll_show_before;

  //创建nodelist
  nodelist := TStringList.Create;

  //创建正则表达式对象
  fpregex := TPerlRegEx.Create;

  //展示树
  showtree;

  Result := True;
end;

function TfrmdbtreebaseMDI.getnodefullbm(nodeid: Integer): string;
var
  qrytmp : TUpAdoQuery;
begin
  Result := '';
  qrytmp := TUpAdoQuery.Create(nil);
  qrytmp.Connection := qry.Connection;

  qrytmp.Close;
  qrytmp.SQL.Text := 'select bianm, fujdbm from ' + tablename + ' where bianm=' + IntToStr(nodeid);
  qrytmp.Open;
  if qrytmp.FieldByName('fujdbm').AsInteger <> 0 then
  begin
    Result := getnodefullbm(qrytmp.fieldbyname('fujdbm').AsInteger) +
       '/' + qrytmp.fieldbyname('bianm').AsString + result;
  end
  else
  begin
    Result := '/' + qrytmp.fieldbyname('bianm').AsString;
  end;
  qrytmp.Close;
  FreeAndNil(qrytmp);

end;

function TfrmdbtreebaseMDI.getnodefullname(nodeid: integer): string;
var
  qrytmp : TUpAdoQuery;
begin
  Result := '';
  qrytmp := TUpAdoQuery.Create(nil);
  qrytmp.Connection := qry.Connection;

  qrytmp.Close;
  qrytmp.SQL.Text := 'select fujdbm, mingc from ' + tablename + ' where bianm=' + IntToStr(nodeid);
  qrytmp.Open;
  if qrytmp.FieldByName('fujdbm').AsInteger <> 0 then
  begin
    Result := getnodefullname(qrytmp.fieldbyname('fujdbm').AsInteger) +
       '/' + qrytmp.fieldbyname('mingc').AsString + result;
  end
  else
  begin
    Result := '/' + qrytmp.fieldbyname('mingc').AsString;
  end;
  qrytmp.Close;
  FreeAndNil(qrytmp);
end;

function TfrmdbtreebaseMDI.getnodename_addchild: string;
begin
  Result := edtmc.Text;
end;

function TfrmdbtreebaseMDI.getnodename_addroot: string;
begin
  Result := edtmc.Text;
end;

function TfrmdbtreebaseMDI.getnodename_showtreenode: string;
begin
  Result := qrytree.FieldByName('mingc').AsString;
end;

function TfrmdbtreebaseMDI.getnodename_updatenode: String;
begin
  Result := edtmc.Text;
end;

procedure TfrmdbtreebaseMDI.showtree;
begin
  tvtree.Items.BeginUpdate;

  //情况树形控件和辅助记录列表
  nodelist.Clear;
  tvtree.items.clear;

  //查询树信息
  if qrytree.Active then qrytree.Close;
  qrytree.SQL.Text := 'SELECT * FROM ' + tablename +
    ' ' + wherestr + ' ORDER BY fujdbm, paixh';
  qrytree.Open;

  qrytree.DisableControls;
  nodelist.Sorted := True;
  qrytree.First;
  while not qrytree.Eof do
  begin
    //显示当前的节点
    showtreenode();

    qrytree.Next;
  end;
  qrytree.EnableControls;
  tvtree.Items.EndUpdate;
end;

procedure TfrmdbtreebaseMDI.showtreenode;
var
  index: integer;
  Node: TTreeNode;
begin
  //根据是否是根节点不同处理
  if qrytree.FieldByName('fujdbm').AsInteger = 0 then
  begin
    //根节点，增加节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
    Node := tvtree.Items.AddChildObject(nil, getnodename_showtreenode,
        qrytree.GetBookmark);
  end
  else
  begin
    //找到父节点的node对象
    Index := nodelist.IndexOf(qrytree.FieldByName('fujdbm').AsString);

    //有时候数据损坏导致父节点不存在,防止程序报错
    if index = -1 then Exit;

    { 2013-5-17：子分类顺序按照倒序修改为AddChildObject
    Node := tvtree.Items.AddChildObjectFirst(TTreeNode(nodelist.Objects[Index]),
         qrytree.FieldByName('mingc').AsString, qrytree.GetBookmark);
    }

    Node := tvtree.Items.AddChildObject(TTreeNode(nodelist.Objects[Index]),
        getnodename_showtreenode, qrytree.GetBookmark);
    //增加子节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
  end;

  //增加当前节点的信息到列表中，以实现在列表中快速查找节点的功能。
  nodelist.AddObject(qrytree.FieldByName('bianm').AsString, Node);
end;

procedure TfrmdbtreebaseMDI.treechange(node: TTreeNode);
begin
  qrytree.GotoBookmark(node.Data);
end;

function TfrmdbtreebaseMDI.treefind(caption: string): Boolean;
var
  i: integer;
begin
  Result := false;

  for i := 0 to tvtree.Items.Count - 1 do
  begin
    fpregex.Subject := tvtree.Items.Item[i].Text;
    fpregex.RegEx := '-?' + caption + '$';
    if fpregex.Match then
    //if tvtree.Items.Item[i].Text = caption then
    begin
      tvtree.Select(tvtree.Items.Item[i]);
      tvtree.SetFocus;
      Result := true;
      exit;
    end;
  end;

end;

function TfrmdbtreebaseMDI.updatenode: boolean;
var
  pid : Integer;  
  bianm : integer;
begin
  Result := False;

  try
    //检查同级别同名分类是否存在
    pid := qrytree.fieldbyname('fujdbm').AsInteger;

    //检查名称是否已经存在，不允许同级别同名重复出现
    if self.dbcheck.colvalueisexists(tablename, 'mingc', getnodename_updatenode,
        'fujdbm=' + IntToStr(pid) + ' and bianm <> ' + qrytree.fieldbyname('bianm').AsString) then
    begin
      errmsg := '同级节点名称[' + getnodename_updatenode + ']已经存在！';
      Exit;
    end;

    //修改节点
    qrytree.Edit;
    UpdateNode_SetFields(pid);
    qrytree.post;  
                 
    //更新当前节点的路径全称
    bianm := qrytree.getInteger('bianm');
    qrytree.Edit;
    qrytree.FieldByName('bianmqm').AsString := getnodefullbm(bianm) + '/';
    qrytree.FieldByName('mingcqm').AsString := getnodefullname(bianm) + '/';
    qrytree.Post;

    Result := True;
  except
    on e:Exception do
    begin
      qrytree.Cancel;
      errmsg := e.Message;
      Result := False;
    end;
  end;
end;

function TfrmdbtreebaseMDI.UpdateNode_SetFields(fujdbm: integer): Boolean;
begin
  qrytree.FieldByName('mingc').AsString := edtmc.Text;
  qrytree.FieldByName('jianp').AsString := baseobj.get_jianpin(edtmc.Text);
  Result := True;
end;

function TfrmdbtreebaseMDI.UpdateNodeAfter: Boolean;
begin
  tvtree.Selected.Text := edtmc.Text;
  Result := True;
end;

function TfrmdbtreebaseMDI.UpdateNodeBefore: Boolean;
begin
  Result := False;

  if tvtree.Selected = nil then Exit;

  edtmc.Text := tvtree.Selected.Text;
  Result := True;
end;

function TfrmdbtreebaseMDI.UpdateNodeCheck: Boolean;
begin
  Result := False;

  if tvtree.Selected = nil then
  begin
    baseobj.showmsg('系统提示', '请选择某个分类!', 0);
    Exit;
  end;

  //名称是否正确
  if Trim(getnodename_updatenode) = '' then
  begin
    baseobj.showmsg('系统提示', '请输入分类名称', 0);
    edtmc.SetFocus;
    Exit;
  end;

  Result := True;
end;

procedure TfrmdbtreebaseMDI.tvtreeChange(Sender: TObject; Node: TTreeNode);
begin
  inherited;

  treechange(node);
end;

procedure TfrmdbtreebaseMDI.tvtreeDblClick(Sender: TObject);
begin
  inherited;

  UpdateNodeBefore;
end;

procedure TfrmdbtreebaseMDI.tvtreeEdited(Sender: TObject; Node: TTreeNode;
  var S: String);
begin
  inherited;

  //节点被修改后，修改数据库
  if not updatenode() then
  begin
    //修改失败
    baseobj.showmsg('', '修改分类名称发生错误：' + errmsg, 0);

    //名称更换为修改前的名称
    s := Node.Text;
  end;
end;

procedure TfrmdbtreebaseMDI.tvtreeGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure TfrmdbtreebaseMDI.btn2Click(Sender: TObject);
begin
  inherited;

  { 添加根分类
  }

  if not addrootnodecheck then Exit;

  //添加根分类
  if not addrootnode() then
  begin
    //添加失败
    baseobj.showmsg('', '添加分类发生错误：' + errmsg, 0);
  end;

  addnodeafter();

  //新增加节点为当前节点
  currrecord_treeselected();
end;

procedure TfrmdbtreebaseMDI.btn3Click(Sender: TObject);
var
  funode : TTreeNode; 
begin
  inherited;

  if not addchildnodecheck then Exit;

  //保存父节点
  funode := tvtree.Selected;

  //增加子节点
  if not addchildnode() then
  begin
    //添加失败
    baseobj.showmsg('', '添加下级分类发生错误：' + errmsg, 0);
  end;

  addnodeafter;

  //将选中节点移动到刚才的父节点
  treechange(funode);
  
  //展开刚才的父节点
  funode.Expanded := True;
end;

procedure TfrmdbtreebaseMDI.btn4Click(Sender: TObject);
begin
  inherited;      

  if not deletecheck() then Exit;
  
  if not DelTree() then
  begin
    //删除商品分类失败
    baseobj.showerror('删除分类发生错误：' + errmsg);
  end;
end;

procedure TfrmdbtreebaseMDI.btn5Click(Sender: TObject);
begin
  inherited;

  if not UpdateNodeCheck then Exit;

  if not updatenode() then
  begin
    //修改失败
    baseobj.showmsg('', '修改分类名称发生错误：' + errmsg, 0);
    Exit;
  end;

  UpdateNodeAfter;
end;

procedure TfrmdbtreebaseMDI.btnfindClick(Sender: TObject);
begin
  inherited;

  if not treefind(Trim(edtcxmc.Text)) then
  begin
    baseobj.showmessage('指定的分类不存在');
  end
end;

procedure TfrmdbtreebaseMDI.edtcxmcKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key=#13 then
  begin
    btnfindClick(btnfind);
  end;
end;

procedure TfrmdbtreebaseMDI.addnodeafter;
begin
  edtyonghbm.Text := '';
  edtmc.Text := '';
  edtpaixh.Text := '0';
end;

procedure TfrmdbtreebaseMDI.btnrefreshClick(Sender: TObject);
begin
  //inherited;

  showtree();
end;

end.
