unit ufrmcomm_process;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, Menus, UpPopupMenu, StdCtrls, ComCtrls, ExtCtrls;

type
  Tfrmcomm_process = class(TForm)
    lblprocess: TLabel;
  private
    { Private declarations }
  public
    procedure setprocessmsg(strmsg : string);
    //设置提示消息

  end;

var
  frmcomm_process: Tfrmcomm_process;

  procedure showProcessMsg(strmsg : string);
  //显示系统提示信息；
  procedure hideProcessMsg();
  //隐藏提示信息；

implementation

{$R *.dfm}

procedure showProcessMsg(strmsg : string);
begin        
  if not Assigned(frmcomm_process) then
    frmcomm_process := tfrmcomm_process.Create(nil);     
  frmcomm_process.show;

  frmcomm_process.setprocessmsg(strmsg + '...');
  Application.ProcessMessages;      
end;

procedure hideProcessMsg();
begin
  frmcomm_process.Close; 
  Application.ProcessMessages;      
end;

{ Tfrmcomm_process }

procedure Tfrmcomm_process.setprocessmsg(strmsg : string);
begin
  lblprocess.Caption := strmsg; 
end;

end.
